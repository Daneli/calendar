import AppRouter from "./routers/AppRouter"

const CalendarApp = () => {
    return (
        <>
            <AppRouter />
        </>
    )
}

export default CalendarApp