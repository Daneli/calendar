import { Calendar } from 'react-big-calendar'
import Navbar from "../components/Navbar"
import 'react-big-calendar/lib/css/react-big-calendar.css'
import { localizer } from '../../helper/calendarLocalizer'
import { getMessagesEs } from '../../helper/getMessages'
import CalendarEvent from '../components/CalendarEvent'
import { useState } from 'react'
import { CalendarModal } from './CalendarModal'
import { useUiStore } from '../../hooks/useUiStore'
import useCalendarStore from '../../hooks/useCalendarStore'
import FabAddNew from '../components/FabAddNew'
import FabDelete from '../components/FabDelete'

const CalendarPage = () => {

    const { openDateModal } = useUiStore()
    const { events, setActiveEvent } = useCalendarStore()
    const [lastView, setLastView] = useState(localStorage.getItem('lastView') || 'week')

    const eventStyleGetter = () => {
        const style = {
            backgroundColor: '#347CF7',
            borderRadius: '0px',
            opacity: 0.8,
            bgColor: 'white'
        }

        return {
            style
        }
    }

    const onDoubleClick = () => {
        openDateModal()
    }

    const onSelect = (event) => {
        setActiveEvent(event)
    }

    const onViewChange = (event) => {
        localStorage.setItem('lastView', event)
        setLastView(event)
    }

    return (
        <>
            <Navbar />

            <Calendar
                culture='es'
                localizer={localizer}
                events={events}
                defaultView={lastView}
                startAccessor="start"
                endAccessor="end"
                style={{ height: 'calc(100vh - 80px)' }}
                messages={getMessagesEs()}
                eventPropGetter={eventStyleGetter}
                components={{
                    event: CalendarEvent
                }}
                onDoubleClickEvent={onDoubleClick}
                onSelectEvent={onSelect}
                onView={onViewChange}
            />

            <CalendarModal />
            <FabAddNew />
            <FabDelete />
        </>
    )
}

export default CalendarPage