
import { Navigate, Route, Routes } from 'react-router-dom'
import CalendarPage from '../calendar/pages/CalendarPage'
import { LoginPage } from '../auth/pages/LoginPage'

const AppRouter = () => {

    const authStatus = 'checking'

    return (
        <Routes>
            {
                (authStatus !== 'not-authenticate'
                    ? <Route path='/auth/*' element={<LoginPage />} />
                    : <Route path='/*' element={<CalendarPage />} />
                )
            }

            <Route path='/*' element={<Navigate to="/auth/login" />} />
        </Routes>
    )
}

export default AppRouter