import { Provider } from "react-redux"
import CalendarApp from "./CalendarApp"
import { BrowserRouter } from "react-router-dom"
import { store } from "./store/store"

function App() {

  return (
    <>
      <Provider store={store}>
        <BrowserRouter>
          <CalendarApp />
        </BrowserRouter>
      </Provider>
    </>
  )
}

export default App
